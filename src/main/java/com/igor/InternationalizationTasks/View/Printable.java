package com.igor.InternationalizationTasks.View;

import java.sql.SQLException;

@FunctionalInterface
public interface Printable {
    void print();
}

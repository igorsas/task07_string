package com.igor.BigStringTasks;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringParser {

    public static void main(String[] args) {
        try {
            //updateText(75, 81);
            String page = getPagesFromTheFile();
            System.out.println("Started page without parsing on classes: \n" + page);
            System.out.println("\n\n");
            List<String> sentences =  getSentences(page);

            List<Sentence> sentencesByClass = converteToListOfSentences(sentences);
            System.out.println("Initial sentences: \n\n" + sentencesByClass);

            System.out.println("\n\nCount sentences with the same words: " + getCountSentencesWithTheSameWords(sentencesByClass));

            /*
            Task 2
             */
            sentencesByClass.sort(Comparator.comparingInt(a -> a.getSentence().size()));
            System.out.println("\n\nSorted sentences: \n\n" + sentencesByClass);

            System.out.println("\n\nFirst sentence: " + sentencesByClass.get(0).getSentence());
            System.out.println("\n\nUniq word from the first sentence: " + getUniqWordFromFirstSentence(sentencesByClass));

            System.out.println("Input word length for question sentences: ");
            int length = new Scanner(System.in).nextInt();
            System.out.println("\n\nUniq words from the question " + getWordsWithLength(page, length));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void updateText(int start, int end) throws IOException {
        String pages = getPages(start, end);
        writeStringInTheFile(pages);
    }

    /*
    Task 1
     */
    private static Integer getCountSentencesWithTheSameWords(List<Sentence> sentencesByClass){
        Set<Integer> idSentencesWithTheSameWords = new HashSet<>();
        for (int i = 0; i < sentencesByClass.size()-1; i++) {
            for(Word wordI : sentencesByClass.get(i).getSentence()){
                for (int j = i+1; j < sentencesByClass.size(); j++) {
                    for(Word wordJ : sentencesByClass.get(j).getSentence()){
                        if(wordI.equals(wordJ)){
                            idSentencesWithTheSameWords.add(i);
                            idSentencesWithTheSameWords.add(j);
                            break;
                        }
                    }
                }
            }
        }
        return idSentencesWithTheSameWords.size();
    }

    /*
    Task 3
     */
    private static String getUniqWordFromFirstSentence(List<Sentence> sentences){
        NextWord: for (Word wordI : sentences.get(0).getSentence()) {
            for (int j = 1; j < sentences.size(); j++) {
                for (Word wordJ : sentences.get(j).getSentence()) {
                    if (wordI.equals(wordJ)) {
                        continue NextWord;
                    }
                }
            }
            return wordI.getWord();
        }
        return "There no uniq words";
    }

    /*
    Task 4
     */
    private static Set<Word> getWordsWithLength(String initialText, int length){
        List<String> sentences =  getQuetionsSentences(initialText);

        List<Sentence> sentenceClass = converteToListOfSentences(sentences);

        Set<Word> uniqWords = new HashSet<>();
        for(Sentence sentence : sentenceClass){
            for(Word word : sentence.getSentence()){
                if(word.getWord().length() == length){
                    uniqWords.add(word);
                }
            }
        }
        return uniqWords;
    }

    private static ArrayList<String> getSentences(String text){
        text = text.trim().replaceAll("[ ,\":+\\-=*₴()<>&%@]+", " ");
        String patNotOnlyDigit = " *[\\d ]+[\\.?!]";
        text = text.replaceAll(patNotOnlyDigit, "");
        String[] sents = text.split(" *[?!\\.] *");

        return new ArrayList<>(Arrays.asList(sents));
    }

    private static ArrayList<String> getQuetionsSentences(String text){
        text = text.trim().replaceAll("[ ,\":+\\-=*₴()<>&%@]+", " ");
        String patNotOnlyDigit = " *[\\d ]+[\\.?!]";
        text = text.replaceAll(patNotOnlyDigit, "");
        String[] sents = text.split(" *[?] *");

        return new ArrayList<>(Arrays.asList(sents));
    }

    private static void parsePunctuationSigns(String s){
        Pattern p = Pattern.compile("\\p{Punct}");

        Matcher m = p.matcher(s);
        int count = 0;
        while (m.find()) {
            count++;
            System.out.println("\nMatch number: " + count);
            System.out.println("start() : " + m.start());
            System.out.println("end()   : " + m.end());
            System.out.println("group() : " + m.group());
        }
    }

    private static void writeStringInTheFile(String page) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter("filename.txt")) {
            out.println(page);
        }
    }

    private static String getPagesFromTheFile() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader ("filename.txt"));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }

    /*
    Read pages from PDF file
     */
    private static String getPages(int start, int end) throws IOException {
        PDFTextStripper pdfStripper = null;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;
        File file = new File("C:/Users/igor_/Downloads/file.pdf");
        // PDFBox 2.0.8 require org.apache.pdfbox.io.RandomAccessRead
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        PDFParser parser = new PDFParser(randomAccessFile);
        parser.parse();
        cosDoc = parser.getDocument();
        pdfStripper = new PDFTextStripper();
        pdDoc = new PDDocument(cosDoc);
        pdfStripper.setStartPage(start);
        pdfStripper.setEndPage(end);
        String parsedText = pdfStripper.getText(pdDoc);
        System.out.println(parsedText);
        return parsedText;
    }

    private static List<Sentence> converteToListOfSentences(List<String> sentences){
        List<Sentence> sentencesByClass = new ArrayList<>(sentences.size());
        for(String s : sentences){
            sentencesByClass.add(new Sentence(s));
        }
        return sentencesByClass;
    }
}

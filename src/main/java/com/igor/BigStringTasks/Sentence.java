package com.igor.BigStringTasks;

import java.util.List;
import java.util.Objects;

public class Sentence {
    @Override
    public String toString() {
        return sentence +
                "\n";
    }

    private List<Word> sentence;

    public Sentence(List<Word> sentence) {
        this.sentence = sentence;
    }

    public Sentence(String sentence){
        this.sentence = Word.parseToWords(sentence);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sentence)) return false;
        Sentence sentence1 = (Sentence) o;
        return Objects.equals(getSentence(), sentence1.getSentence());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSentence());
    }

    public List<Word> getSentence() {
        return sentence;
    }

    public void setSentence(List<Word> sentence) {
        this.sentence = sentence;
    }
}

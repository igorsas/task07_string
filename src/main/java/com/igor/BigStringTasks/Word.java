package com.igor.BigStringTasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Word {
    private String word;

    public Word(String word) {
        this.word = word;
    }

    public static List<Word> parseToWords(String sentence){
        List<Word> words = new ArrayList<>();
        String pattern = "[\\w]+";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(sentence);
        while (m.find()) {
            String word = sentence.substring(m.start(), m.end());
            words.add(new Word(word));
        }
        return words;
    }


    public void setWord(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;
        Word word1 = (Word) o;
        return Objects.equals(getWord(), word1.getWord());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getWord());
    }

    @Override
    public String toString(){
        return word;
    }
}

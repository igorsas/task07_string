package com.igor.RegexTasks;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws IOException {
        String sentence;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter your sentence");
        sentence = reader.readLine();
        System.out.println("Your sentence " + (isBeginFromCapitalLetter(sentence) ? "started with a capital letter" : "is incorrect"));

        String patNotOnlyDigit = "[aeiouAEIOU]";
        sentence = sentence.replaceAll(patNotOnlyDigit, "_");
        System.out.println("\nSentence without vowel letters \n" + sentence);
    }

    private static boolean isBeginFromCapitalLetter(String sentence){
        String pattern = "^[A-Z]";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(sentence);
        while (m.find()) {
            return true;
        }
        return false;
    }
}
